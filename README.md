# Readme

![Codacy grade](https://imgstyle=for-the-badge)
![Bitbucket Pipelines](https://img.shields.io/bitbucket/pipelines/asmaru/di/master?style=for-the-badge)
![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg?style=for-the-badge)

A Mustache implementation in PHP.

## Usage ##

```
#!php
<?php
$template = 'Hello {{planet}}';
$context = new \asmaru\mustache\Context(['planet' => 'World!']);
$parser = new \asmaru\mustache\MustacheParser();
echo $parser->render($template, $context);
```