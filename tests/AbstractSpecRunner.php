<?php

namespace asmaru\mustache;

use PHPUnit\Framework\TestCase;

class AbstractSpecRunner extends TestCase {

	protected function runTestSpec($spec) {
		$parser = new MustacheParser();
		$tests = file_get_contents(__DIR__ . DIRECTORY_SEPARATOR . 'specs' . DIRECTORY_SEPARATOR . $spec);
		$tests = json_decode($tests, null, 512, JSON_THROW_ON_ERROR);
		foreach ($tests->tests as $test) {
			echo 'name: ' . $test->name . PHP_EOL;
			echo 'desc: ' . $test->desc . PHP_EOL;
			echo 'template: "' . $test->template . '"' . PHP_EOL;
			echo 'expected: "' . $test->expected . '"' . PHP_EOL;
			$data = (array)$test->data;
			if (isset($data['lambda'])) {
				$data['lambda'] = fn($text) => eval($data['lambda']->php);
			}
			if (isset($test->partials)) {
				$data = array_merge_recursive($data, (array)$test->partials);
			}
			$context = new Context($data);
			$result = $parser->render($test->template, $context);
			echo 'result:   "' . $result . '"' . PHP_EOL;
			$this->assertEquals($test->expected, $result);
			echo PHP_EOL;
		}
	}
}