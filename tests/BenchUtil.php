<?php
namespace asmaru\mustache;

class BenchUtil {
	private $start;

	public function __construct() {
		$this->start = microtime(true);
	}

	public static function start() {
		return new self();
	}

	public function end() {
		$end = microtime(true);
		$time = round(1000 * ($end - $this->start), 2);
		echo 'runtime: ' . $time . 'ms' . PHP_EOL;
		return $time;
	}
}