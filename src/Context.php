<?php

namespace asmaru\mustache;

use ArrayAccess;

class Context {

	final const SEPARATOR = '.';

	private mixed $values;

	/**
	 * @param mixed $values
	 * @param Context|null $parent
	 */
	public function __construct(mixed $values, private ?Context &$parent = null) {
		$this->values = is_object($values) && !($values instanceof ArrayAccess) ? (array)$values : $values;
	}

	/**
	 * @param $name
	 *
	 * @return mixed
	 */
	public function get($name) {
		// implicit iterator value
		if ($name === self::SEPARATOR && !empty($this->values)) return $this->values;

		// resolve the name
		$parts = explode(self::SEPARATOR, (string)$name);

		// if the key is not found in the current context ascend to the parent context
		if (!isset($this->values[$parts[0]])) {
			if ($this->parent !== null) {
				return $this->parent->get($name);
			} else return null;
		}

		$value = $this->values[$parts[0]];

		// if only one part is left return the value
		if (count($parts) === 1) {
			return $value;
		} else {
			// there are more parts left, descend to child context
			if (array_shift($parts) === null) return null;
			return (new Context($value))->get(implode(self::SEPARATOR, $parts));
		}
	}
}