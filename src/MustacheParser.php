<?php

namespace asmaru\mustache;

use asmaru\mustache\node\T_COMMENT;
use asmaru\mustache\node\T_INVERTED_SECTION_OPEN;
use asmaru\mustache\node\T_LINEBREAK;
use asmaru\mustache\node\T_NODE;
use asmaru\mustache\node\T_PARTIAL;
use asmaru\mustache\node\T_ROOT;
use asmaru\mustache\node\T_SECTION_CLOSE;
use asmaru\mustache\node\T_SECTION_OPEN;
use asmaru\mustache\node\T_STATIC;
use asmaru\mustache\node\T_VAR;
use asmaru\mustache\node\T_VAR_RAW;
use asmaru\mustache\node\T_WHITESPACE;

class MustacheParser {

	/** @var array */
	protected $tokens = [
		T_LINEBREAK::class,
		T_WHITESPACE::class,
		T_STATIC::class,
		T_COMMENT::class,
		T_PARTIAL::class,
		T_INVERTED_SECTION_OPEN::class,
		T_SECTION_OPEN::class,
		T_SECTION_CLOSE::class,
		T_VAR_RAW::class,
		T_VAR::class
	];

	/**
  * @param string $template
  * @return string
  */
 public function render($template, mixed $context) {
		if (!$context instanceof Context) {
			$context = new Context($context);
		}
		return $this->compile($template)->render($context);
	}

	/**
	 * @param string $template
	 * @return T_NODE
	 */
	public function compile($template) {
		return $this->parse($this->scan($template));
	}

	/**
  * @return T_NODE
  */
 private function parse(array $nodes) {
		$root = new T_ROOT();
		$current =& $root;
		$parsedNodes = [];
		/** @var T_NODE $node */
		$length = count($nodes);
		for ($i = 0; $i < $length; $i++) {
			$node = $nodes[$i];
			$node->parent = $current;
			if ($node instanceof T_SECTION_OPEN || $node instanceof T_INVERTED_SECTION_OPEN) {
				// open section
				$current->children[] = $node;
				$current = $node;
			} else if ($node instanceof T_SECTION_CLOSE) {
				// close section
				$current = $current->parent;
			} else {
				// same level
				$current->children[] = $node;
			}

			$isLast = ($i + 1) === $length;

			$this->cleanWhitespace($node, $parsedNodes, $isLast);

			$parsedNodes[] = $node;
		}
		return $root;
	}

	/**
  * @param $isLast
  */
 protected function cleanWhitespace(T_NODE &$node, array &$nodes, $isLast) {
		$lastIndex1 = count($nodes) - 1;
		$lastIndex2 = $lastIndex1 - 1;
		$lastIndex3 = $lastIndex2 - 1;

		$lastNode = $nodes[$lastIndex1] ?? null;
		$lastNodePrevious = $nodes[$lastIndex2] ?? null;
		$lastNodePrevious2 = $nodes[$lastIndex3] ?? null;

		// last (hack ?????)
		if ($isLast) {
			if ($node instanceof T_SECTION_OPEN || $node instanceof T_SECTION_CLOSE || $node instanceof T_COMMENT || $node instanceof T_PARTIAL || $node instanceof T_INVERTED_SECTION_OPEN) {
				if ($lastNode !== null) {
					if ($lastNode instanceof T_WHITESPACE && $lastNodePrevious instanceof T_LINEBREAK) {
						$lastNode->remove();
					}
				}
			}
		}

		// remove whitespace and linebreak from standalone tags
		if ($node instanceof T_LINEBREAK) {
			if ($lastNode !== null) {
				// previous Node is section
				if ($lastNode instanceof T_SECTION_OPEN || $lastNode instanceof T_SECTION_CLOSE || $lastNode instanceof T_COMMENT || $lastNode instanceof T_PARTIAL || $lastNode instanceof T_INVERTED_SECTION_OPEN) {

					// standalone line
					if ($lastNodePrevious === null || $lastNodePrevious instanceof T_LINEBREAK) {
						$node->remove();
					}

					// standalone line
					if ($lastNodePrevious instanceof T_WHITESPACE) {
						if ($lastNodePrevious2 === null || $lastNodePrevious2 instanceof T_LINEBREAK) {
							$lastNodePrevious->remove();
							$node->remove();
						}
					}
				}
			}
		}
	}

	/**
	 * @param $template
	 * @return array
	 */
	private function scan(&$template) {
		$nodes = [];
		while ($node = $this->match($template)) {
			$nodes[] = $node;
		}
		return $nodes;
	}

	/**
	 * @param $template
	 * @return T_NODE
	 */
	private function match(&$template) {
		if (!empty($template)) {
			foreach ($this->tokens as $token) {
				$node = $token::consume($template);
				if ($node !== null) {
					return $node;
				}
			}
		}
		return null;
	}
}