<?php

namespace asmaru\mustache;

/**
 * Interface Helper
 *
 * @package asmaru\mustache
 */
interface Helper {

	public function render($content, Context $context, array $params = []);

	public function renderBefore(): bool;
}