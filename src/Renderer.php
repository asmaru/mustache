<?php

namespace asmaru\mustache;

use Exception;
use function mb_strtolower;
use ReflectionClass;
use ReflectionException;

class Renderer {

	private array $context = ['partials' => []];

	/**
	 * @var Helper[]
	 */
	private array $helpers = [];

	/**
  * Renderer constructor.
  */
 public function __construct(private readonly Parser $parser)
 {
 }

	public function addHelper(Helper $viewHelper) {
		$this->helpers[] = $viewHelper;
	}

	/**
	 * @param $name
	 * @param $template
	 */
	public function addPartial($name, $template) {
		$this->context['partials'][$name] = $template;
	}

	/**
	 * @param $name
	 * @return string|null
	 */
	public function getPartial($name) {
		return $this->context['partials'][$name] ?? null;
	}

	/**
	 * @param $name
	 * @param $value
	 */
	public function assign($name, $value) {
		$this->context[$name] = $value;
	}

	public function assignAll(array $values) {
		$this->context = array_merge($this->context, $values);
	}

	/**
	 * @param $template
	 * @return string
	 * @throws Exception
	 */
	public function render($template) {
		foreach ($this->helpers as $helper) {
			$this->prepareHelper($helper);
		}
		return $this->parser->render($template, $this->context);
	}

	/**
  * @throws Exception
  */
 private function prepareHelper(Helper $helper) {
		$name = $this->getHelperShortName($helper);
		if (isset($this->context[$name])) {
			//throw new Exception(sprintf('duplicate key "%s"', $name));
			return;
		}
		$this->context[$name] = function ($content, $params, Context $context) use (&$helper, $name) {
			if ($helper->renderBefore()) {
				$content = $this->parser->render($content, $context);
			}
			return $helper->render($content, $context, $params);
		};
	}

	/**
  * @return string
  * @throws ReflectionException
  */
 private function getHelperShortName(Helper $helper) {
		$name = lcfirst((new ReflectionClass($helper))->getShortName());
		return mb_substr($name, 0, mb_stripos($name, 'Helper'));
	}
}