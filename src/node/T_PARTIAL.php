<?php

namespace asmaru\mustache\node;

use asmaru\mustache\Context;
use asmaru\mustache\Parser;

class T_PARTIAL extends T_NODE {

	protected static $REGEX = '/^\{\{>([^\}\r\n]+)\}\}/S';

	public function render(Context $context) {
		$value = $context->get(trim($this->content));
		if (!empty($value)) {
			$previous = $this->getPrevious();

			// indent if whitespace is present
			if ($previous instanceof T_WHITESPACE) {
				$previous2 = $previous->getPrevious();
				if ($previous2 === null || $previous2 instanceof T_LINEBREAK) {
					$whitespace = $previous->rawContent;

					// indent first line
					$value = $whitespace . $value;

					// indent other lines
					$value = preg_replace_callback('/([\n\r]+)/S', fn($matches) => $matches[1] . $whitespace, $value);

					// remove whitespace after last linebreak
					$value = preg_replace_callback('/([\n\r]+)([ \t\f]+)$/S', fn($matches) => $matches[1], $value);
				}
			}
			return (new Parser())->render($value, $context);
		} else return '';
	}
}