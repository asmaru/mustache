<?php
namespace asmaru\mustache\node;

use asmaru\mustache\Context;

class T_COMMENT extends T_NODE {

	protected static $REGEX = '/^\{\{!([^\}]+)\}\}/S';

	public function render(Context $context) {
		return '';
	}
}