<?php

namespace asmaru\mustache\node;

use asmaru\mustache\Context;

class T_WHITESPACE extends T_NODE {

	protected static $REGEX = '/^([\t\f ]+)/S';

	public function render(Context $context) {
		return $this->content;
	}
}
