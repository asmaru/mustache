<?php


namespace asmaru\mustache\node;


use asmaru\mustache\Context;

class T_ROOT extends T_NODE {
	public function render(Context $context) {
		$result = '';
		/** @var T_NODE $node */
		foreach ($this->children as $node) {
			$result .= $node->render($context);
		}
		return $result;
	}
}