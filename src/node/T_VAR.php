<?php

namespace asmaru\mustache\node;

use asmaru\mustache\Context;
use asmaru\mustache\Parser;

class T_VAR extends T_NODE {

	protected static $REGEX = '/^\{\{([^\}\r\n]+)\}\}/S';

	public function render(Context $context) {
		$escape = true;
		$name = null;
		if (preg_match('/^&(.*)$/', $this->content, $matches) === 1) {
			$escape = false;
			$name = trim($matches[1]);
		} else {
			$name = trim($this->content);
		}

		$value = $context->get($name);

		if (is_callable($value)) {
			$value = (new Parser())->render($value(null, [], $context), $context);
		}
		return $escape ? htmlentities((string) $value, ENT_COMPAT, 'UTF-8') : $value;
	}
}
