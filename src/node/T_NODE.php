<?php

namespace asmaru\mustache\node;

use asmaru\mustache\Context;

abstract class T_NODE {

	/** @var string */
	protected static $REGEX = '';
	/** @var string */
	public $content;
	/** @var T_NODE */
	public $parent;
	/** @var array */
	public $children = [];
	/** @var string */
	public $rawContent;

	/**
	 * @param $template
	 * @return T_NODE
	 */
	public static function consume(&$template) {
		if (preg_match(static::$REGEX, (string) $template, $matches) === 1) {
			$template = substr((string) $template, strlen($matches[0]));
			$node = new static();
			$node->content = $matches[1];
			$node->rawContent = $matches[0];
			return $node;
		}
		return null;
	}

	/**
  * @return string
  */
 abstract public function render(Context $context);

	public function remove() {
		$this->content = '';
	}

	/**
	 * @return string
	 */
	public function getRawContent() {
		if (!empty($this->children)) {
			$content = '';
			/** @var T_NODE $node */
			foreach ($this->children as $node) {
				$content .= $node->getRawContent();
			}
			return $content;
		} else return $this->rawContent;
	}

	/**
	 * @return T_NODE
	 */
	protected function getPrevious() {
		$children = $this->parent->children;
		$count = count($children);
		for ($i = 0; $i < $count; $i++) {
			if ($children[$i] === $this && isset($children[$i - 1])) {
				return $children[$i - 1];
			}
		}
		return null;
	}
}