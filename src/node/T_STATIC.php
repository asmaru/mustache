<?php

namespace asmaru\mustache\node;

use asmaru\mustache\Context;

class T_STATIC extends T_NODE {

	private static array $endLimiters = ["\r\n", "\n", "\r", "{{"];

	public static function consume(&$template) {
		$end = strlen((string) $template);
		foreach (self::$endLimiters as $limiter) {
			$p = strpos((string) $template, (string) $limiter);
			// delimiter found -> cancel
			if ($p === 0) return null;

			if ($p > 0) {
				// delimiter found and not at first place
				if ($p < $end) {
					// first delimiter
					$end = $p;
				}
			}
		}

		$value = substr((string) $template, 0, $end);
		$template = substr((string) $template, $end);

		$node = new static();
		$node->content = $value;
		$node->rawContent = $value;
		return $node;
	}

	public function render(Context $context) {
		return $this->content;
	}
}
