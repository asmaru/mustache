<?php

namespace asmaru\mustache\node;


use asmaru\mustache\Context;
use asmaru\mustache\Parser;

class T_INVERTED_SECTION_OPEN extends T_NODE {

	protected static $REGEX = '/^\{\{\^([^\}\r\n]+)\}\}/S';

	public function render(Context $context) {
		$name = trim($this->content);
		$value = $context->get($name);
		if (is_callable($value)) {
			return (new Parser())->render($value($this->getRawContent()), $context);
		}
		if (empty($value)) {
			$result = '';
			$count = count($this->children);
			for ($i = 0; $i < $count; $i++) {
				/** @var T_NODE $node */
				$node = $this->children[$i];
				$result .= $node->render($context);
			}
			return $result;
		} else return '';
	}
}