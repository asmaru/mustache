<?php

namespace asmaru\mustache\node;

use ArrayAccess;
use asmaru\mustache\Context;
use Iterator;

class T_SECTION_OPEN extends T_NODE {

	protected static $REGEX = '/^\{\{#([^\}\r\n]+)\}\}/S';

	public function render(Context $context) {
		$name = trim($this->content);

		// parse section params
		$params = [];
		if (preg_match('/^([^(]+)\(([^\)]+)\)$/', $name, $matches)) {
			$name = $matches[1];
			$params = $matches[2];
			parse_str(($params), $params);
		}

		$value = $context->get($name);

		// call lambda
		if (is_callable($value)) {
			$value = $value($this->getRawContent(), $params, $context);
			if (!is_array($value)) {
				return $value;
			}
		}

		if (!empty($value) && $value !== false) {

			// check if the value is an array and wrap it in an array if not
			$isArray = is_array($value) || $value instanceof Iterator;
			if (!$isArray) {
				$value = [$value];
			}

			$content = [];

			foreach ($value as $key => $item) {
				$itemContext = new Context($item, $context);
				$count = count($this->children);
				for ($i = 0; $i < $count; $i++) {
					/** @var T_NODE $node */
					$node = $this->children[$i];
					$content[] = $node->render($itemContext);
				}
			}
			return implode('', $content);
		} else return '';
	}
}