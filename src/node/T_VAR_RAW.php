<?php

namespace asmaru\mustache\node;

use asmaru\mustache\Context;
use asmaru\mustache\Parser;

class T_VAR_RAW extends T_NODE {

	protected static $REGEX = '/^\{\{\{([^\}\r\n]+)\}\}\}/S';

	public function render(Context $context) {
		$value = $context->get(trim($this->content));
		if (is_callable($value)) {
			$value = (new Parser())->render($value(null), $context);
		}
		return $value;
	}
}