<?php

namespace asmaru\mustache\node;

use asmaru\mustache\Context;

class T_SECTION_CLOSE extends T_NODE {

	protected static $REGEX = '/^\{\{\/([^\}\r\n]+)\}\}/S';

	public function render(Context $context) {
		return '';
	}
}
