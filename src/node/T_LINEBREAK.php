<?php

namespace asmaru\mustache\node;

use asmaru\mustache\Context;

class T_LINEBREAK extends T_NODE {

	protected static $REGEX = '/^([\n\r]+)/S';

	public function render(Context $context) {
		return $this->content;
	}
}
